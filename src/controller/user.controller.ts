import { Controller, Get, Post, Body } from '@nestjs/common';
import { UserService } from '../service/user.service';
import { User } from '..//model/user';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Get()
  getAllUsers() {
    return this.userService.getUsers();
  }

  @Post()
  createUser(@Body() userBody: User) {
    return this.userService.createUser(userBody);
  }
}
