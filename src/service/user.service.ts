import { Injectable } from '@nestjs/common';
import { UserRepository } from '../repository/user.repository';
import { User } from '../model/user';
@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) { }

  createUser(userObj: User): any {
    const user = new User(userObj);
    const result = this.userRepository.create(user);
    return result;
  }

  getUsers() {
    return this.userRepository.findAll();
  }
}
