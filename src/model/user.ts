import { Injectable } from '@nestjs/common';

@Injectable()
export class User {
    constructor({
        firstName,
        lastName,
        age,
    }) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    firstName: string;
    lastName: string;
    age: number;
}
