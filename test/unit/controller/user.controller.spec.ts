import { UserController } from '../../../src/controller/user.controller';
import { UserService } from '../../../src/service/user.service';
import { UserRepository } from '../../../src/repository/user.repository';

describe('User Controller', () => {
  let userController: UserController;
  let userService: UserService;
  let userRepository: UserRepository;

  beforeEach(() => {
    userRepository = new UserRepository('');
    userService = new UserService(userRepository);
    userController = new UserController(userService);
  });

  it('Should retrieve all users from db', async () => {
    const query = [
      {
        _id: '5cb7eb24a1f5543364b8a236',
        firstName: 'leo2',
        lastName: 'teste2',
        age: 23,
        __v: 0,
      },
      {
        _id: '5cb7eb2ba1f5543364b8a237',
        firstName: 'leo1',
        lastName: 'teste1',
        age: 23,
        __v: 0,
      },
    ];

    userRepository.findAll = jest.fn(() => Promise.resolve(query));

    expect(await userController.getAllUsers()).toBe(query);
  });
});
