import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { User } from '../model/user';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class UserRepository {
    constructor(@InjectModel('User') private readonly userModel: Model<User>) { }

    async create(user: User): Promise<User> {
        const createdUser = new this.userModel(user);
        return await createdUser.save();
    }

    findAll(): Promise<User[]> {
        return this.userModel.find().exec();
    }
}
